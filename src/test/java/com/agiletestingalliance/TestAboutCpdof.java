package com.agiletestingalliance;

import static org.mockito.Mockito.*;
import org.junit.Test;
import static org.junit.Assert.*;


public class TestAboutCpdof {

	@Test
	public void testCpDof() throws Exception {
		String  result = new AboutCPDOF().desc();
		boolean foundIt = result.contains("CP-DOF certification program covers end to end");
		assertEquals("String not found",foundIt, true);
	}
}
