package com.agiletestingalliance;

import static org.mockito.Mockito.*;
import org.junit.Test;
import static org.junit.Assert.*;


public class MinMaxTest {

	@Test
	public void testMinMaxFirst() throws Exception {
		int result = new MinMax().fading(2,4);
		assertEquals("New Test Failed not 4",result, 4);
	}

	@Test
	public void testMinMaxSecond() throws Exception {
		int result = new MinMax().fading(4,2);
		assertEquals("New Test Failed not 4",result, 4);
	}
}
