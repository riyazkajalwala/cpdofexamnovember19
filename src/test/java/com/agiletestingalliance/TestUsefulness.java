package com.agiletestingalliance;

import static org.mockito.Mockito.*;
import org.junit.Test;
import static org.junit.Assert.*;


public class TestUsefulness {

	@Test
	public void testUsefulness() throws Exception {
		String  result = new Usefulness().desc();
		boolean foundIt = result.contains("transformation");
		assertEquals("String not found",foundIt, true);
	}
}
